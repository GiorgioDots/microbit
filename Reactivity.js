let servoB = 0
let servoA = 0
let b = 0
let a = 0
let stato = 0
input.onPinPressed(TouchPin.P0, function () {
    if (a == 5 || b == 5) {
        a = 0
        servoA = 180
        pins.servoWritePin(AnalogPin.P8, servoA)
        b = 0
        servoB = 170
        pins.servoWritePin(AnalogPin.P16, servoB)
    }
    basic.showNumber(a)
    basic.showLeds(`
        . . . . .
        . . . . .
        . . . . .
        . . . . .
        . . . . .
        `)
    basic.showNumber(b)
    basic.showLeds(`
        . . . . .
        . . . . .
        . . . . .
        . . . . .
        . . . . .
        `)
    stato = 0
    basic.showIcon(IconNames.Chessboard)
    basic.clearScreen()
    basic.pause(2000 + Math.randomRange(0, 2000))
    if (stato == 0) {
        led.plot(2, 2)
    }
    stato = 1
})
input.onPinPressed(TouchPin.P2, function () {
    led.unplot(2, 2)
    if (stato == 1) {
        led.plot(3, 1)
        led.plot(4, 2)
        led.plot(3, 3)
        b += 1
        servoB += -36
        pins.servoWritePin(AnalogPin.P16, servoB)
    } else if (stato != 2) {
        led.plot(1, 1)
        led.plot(0, 2)
        led.plot(1, 3)
        a += 1
        servoA += -36
        pins.servoWritePin(AnalogPin.P8, servoA)
    }
    stato = 2
})
input.onPinPressed(TouchPin.P1, function () {
    led.unplot(2, 2)
    if (stato == 1) {
        led.plot(1, 1)
        led.plot(0, 2)
        led.plot(1, 3)
        a += 1
        servoA += -36
        pins.servoWritePin(AnalogPin.P8, servoA)
    } else if (stato != 2) {
        led.plot(3, 1)
        led.plot(4, 2)
        led.plot(3, 3)
        b += 1
        servoB += -36
        pins.servoWritePin(AnalogPin.P16, servoB)
    }
    stato = 2
})
stato = 0
a = 0
b = 0
servoA = 180
servoB = 170
pins.servoWritePin(AnalogPin.P8, servoA)
pins.servoWritePin(AnalogPin.P16, servoB)
